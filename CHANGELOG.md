## [1.5.1](https://gitlab.com/to-be-continuous/sbt/compare/1.5.0...1.5.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([a9da042](https://gitlab.com/to-be-continuous/sbt/commit/a9da0428a80882829c3c4befacebc67fcc6584be))

# [1.5.0](https://gitlab.com/to-be-continuous/sbt/compare/1.4.0...1.5.0) (2024-1-27)


### Features

* migrate to CI/CD component ([819d754](https://gitlab.com/to-be-continuous/sbt/commit/819d754744895a8cacbb0abd83d0a0f86e5ed369))

# [1.4.0](https://gitlab.com/to-be-continuous/sbt/compare/1.3.1...1.4.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([d740f3e](https://gitlab.com/to-be-continuous/sbt/commit/d740f3ee304ff60060c25b456a51e58ab2972bfd))

## [1.3.1](https://gitlab.com/to-be-continuous/sbt/compare/1.3.0...1.3.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([0e1214d](https://gitlab.com/to-be-continuous/sbt/commit/0e1214de03ba09c93595a182b1f6f3fc0c985c20))

# [1.3.0](https://gitlab.com/to-be-continuous/sbt/compare/1.2.0...1.3.0) (2023-05-28)


### Features

* **release:** normalize release & publish ([16c7d94](https://gitlab.com/to-be-continuous/sbt/commit/16c7d940529c32de0d1463deec90c6e9c0e7844c))

# [1.2.0](https://gitlab.com/to-be-continuous/sbt/compare/1.1.2...1.2.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([b756717](https://gitlab.com/to-be-continuous/sbt/commit/b7567173d547b55c756b2334bb2b56f9e8c3077a))

## [1.1.2](https://gitlab.com/to-be-continuous/sbt/compare/1.1.1...1.1.2) (2023-03-28)


### Bug Fixes

* **sbom:** add CycloneDX report ([914eee2](https://gitlab.com/to-be-continuous/sbt/commit/914eee2132de57e54ff96b1310bce80b69e191f6))

## [1.1.1](https://gitlab.com/to-be-continuous/sbt/compare/1.1.0...1.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([663c9c6](https://gitlab.com/to-be-continuous/sbt/commit/663c9c688ddd0862938510e7a5328afdc346b964))

# [1.1.0](https://gitlab.com/to-be-continuous/sbt/compare/1.0.0...1.1.0) (2022-12-19)


### Features

* add a job generating software bill of materials ([efcaf0f](https://gitlab.com/to-be-continuous/sbt/commit/efcaf0fd136248d686f6964a20cf98d734241399))

# 1.0.0 (2022-08-21)


### Features

* initial template version ([926ba1d](https://gitlab.com/to-be-continuous/sbt/commit/926ba1d0100a1a6dc4162ef56d7573267fb05a53))
